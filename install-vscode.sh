#!/bin/bash
set -e

mkdir -p /tmp/packages
mkdir -p /tmp/download

# Dependencies
wget -O /tmp/download/libnss3.deb http://security.ubuntu.com/ubuntu/pool/main/n/nss/libnss3_3.98-0ubuntu0.22.04.2_amd64.deb
dpkg -x /tmp/download/libnss3.deb /tmp/packages

wget -O /tmp/download/libnspr4.deb http://security.ubuntu.com/ubuntu/pool/main/n/nspr/libnspr4_4.35-0ubuntu0.22.04.1_amd64.deb
dpkg -x /tmp/download/libnspr4.deb /tmp/packages

wget -O /tmp/download/libgtk-3-0.deb http://security.ubuntu.com/ubuntu/pool/main/g/gtk+3.0/libgtk-3-0_3.24.33-1ubuntu2.1_amd64.deb
dpkg -x /tmp/download/libgtk-3-0.deb /tmp/packages

wget -O /tmp/download/libatk1.0-0.deb http://fr.archive.ubuntu.com/ubuntu/pool/main/a/atk1.0/libatk1.0-0_2.36.0-3build1_amd64.deb
dpkg -x /tmp/download/libatk1.0-0.deb /tmp/packages

wget -O /tmp/download/libatk-bridge2.0-0.deb http://fr.archive.ubuntu.com/ubuntu/pool/main/a/at-spi2-atk/libatk-bridge2.0-0_2.38.0-3_amd64.deb
dpkg -x /tmp/download/libatk-bridge2.0-0.deb /tmp/packages

wget -O /tmp/download/libxcomposite1.deb http://fr.archive.ubuntu.com/ubuntu/pool/main/libx/libxcomposite/libxcomposite1_0.4.5-1build2_amd64.deb
dpkg -x /tmp/download/libxcomposite1.deb /tmp/packages

wget -O /tmp/download/libxdamage1.deb http://fr.archive.ubuntu.com/ubuntu/pool/main/libx/libxdamage/libxdamage1_1.1.5-2build2_amd64.deb
dpkg -x /tmp/download/libxdamage1.deb /tmp/packages

wget -O /tmp/download/libatspi.deb http://fr.archive.ubuntu.com/ubuntu/pool/main/a/at-spi2-core/libatspi2.0-0_2.44.0-3_amd64.deb
dpkg -x /tmp/download/libatspi.deb /tmp/packages

wget -O /tmp/download/libepoxy0.deb http://fr.archive.ubuntu.com/ubuntu/pool/main/libe/libepoxy/libepoxy0_1.5.10-1_amd64.deb
dpkg -x /tmp/download/libepoxy0.deb /tmp/packages

# wget -O /tmp/download/libsecret.deb http://fr.archive.ubuntu.com/ubuntu/pool/main/libs/libsecret/libsecret-1-0_0.20.5-2_amd64.deb
# dpkg -x /tmp/download/libsecret.deb /tmp/packages

# VSCode
wget -O /tmp/download/code.deb "https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64"
dpkg -x /tmp/download/code.deb /tmp/packages