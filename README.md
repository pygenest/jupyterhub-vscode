# VSCode for JupyterHub

Install Visual Studio Code in a PAGODA JupyterHub VM and access it remotely using Remote Tunnels.

*:warning: Warning: as we do not have admin access to the VM, we have to tinker with package installation. Ideally it would be a good idea to provide a special image with VSCode already installed.*

## Installation

*Installation should be performed once at the startup of the VM.*

Open a JupyterHub terminal.

Edit the `~/.bashrc` file (e.g., `vim ~/.bashrc`).
At the end of the file, add the content of this repository's `bashrc`.
It defines environment variables for the path, libraries, and proxy.

**Exit the terminal (using `exit`), and reopen a new terminal!**

Launch the `install-vscode.sh` script using `bash`:
```shell
bash install-vscode.sh
```

## Usage

Open a JupyterHub terminal.

Launch the `launch-vscode.sh` script using `bash`:
```shell
bash launch-vscode.sh
```

The first time you use this script, you should be asked to "grant access to the server" (see below).
Open the link in your browser, and fill the code.

![](assets/launch.png)

Your remote is now ready!
*You can close the terminal tab, but do not exit it.*

<br/>

With your local computer, launch VSCode and install the extension [Remote Tunnels](https://code.visualstudio.com/docs/remote/tunnels).

In the Remote Explorer tab, click on `Tunnels +`, choose GitHub, and then choose `jupyter-pagoda`.

![](assets/remote-tunnels.png)