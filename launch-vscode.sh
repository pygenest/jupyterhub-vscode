#!/bin/bash
set -e

mkdir -p /tmp/vscode/server
code tunnel --accept-server-license-terms --server-data-dir=/tmp/vscode/server --name="jupyter-pagoda"